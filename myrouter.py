# This file is an example pox instance configuration for the logic
#  present in a router as applied to an openflow device in a mininet
#  network. The Mininet topology is mytopo.py - a single OF switch and
#  3 hosts. The goal implementation will incoorperate arp and icmp
#  processing on the device, and layer 3 routing across the network.
# Also included is a primitive, static routing table.

# Research
#  https://github.com/mininet/openflow-tutorial/wiki/Router-Exercise        (Pox/mn/OF Router Exercise)
#  https://pieknywidok.blogspot.co.uk/2012/08/arp-and-ping-in-pox-building-pox-based.html (ARP + ICMP Response Example)
#  http://www.tcpipguide.com/free/t_ARPAddressSpecificationandGeneralOperation-2.htm      (ARP)
#  http://www.tcpipguide.com/free/t_ARPCaching.htm                          (ARP Caching)
#  http://archive.openflow.org/doc/gui/org/openflow/protocol/Match.html     (match obj specification)
#  http://rlenglet.github.io/openfaucet/match.html                          (match obj explanation)
#  http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml  (Ly3 Proto numbers re. match.nw_proto)

# THE COIN DROPPED
#  Every router in this POX instance (i.e. Every instantiated basic_router object), can use
#  a SINGLE, GLOBALISED ROUTING TABLE. 

from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as lib_packet
from pox.lib.packet.arp import arp              # 'arp' object
from pox.lib.packet.ethernet import ethernet    # 'ethernet' object
from pox.lib.packet.icmp import icmp            # 'icmp' object
from pox.lib.addresses import IPAddr, EthAddr, cidr_to_netmask  # Fns to produce address types

SUPPRESS_MBCAST_MSG = True
SUPPRESS_PACKET_MSG = True
FLOW_IDLE_TIMEOUT = 10


# Basic Design:
#  table_network_struct is loaded with some values and passed to an
#  ipv4_static_route_table instance, effectively installing a network
#  into a globalised routing table.





# Closest thing to a struct like in C. 
# Used to contain a network for ipv4_static_route_table
class table_network_struct(object):
  """
  NOTES:
   netmask_to_cidr() is defined in pox.lib.addresses
    255.255.255.0 -> 24
  """
  tar_net_addr    = None  # network address for added network
  mask_cidr       = None  # said networks mask val e.g. 24
  switch_id       = None  # UID for OF Switch
  egress_if       = None  # port, in of nomenclature
  egress_if_addr  = None  # said ports ip
  
  def __init__(self, tar_net_addr=None, mask_cidr=None, switch=None, 
    egress_if=None, egress_if_addr=None):
    
    self.tar_net_addr     = tar_net_addr
    self.mask_cidr        = mask_cidr
    self.switch_id        = ""
    self.egress_if        = egress_if
    self.egress_if_addr   = egress_if_addr



# ISSUE
# You can store router specific attributes in a global routing table!
# Fix this!

class ipv4_static_route_table(object):

  table = []
  
  #def add_network(self, tar_net_addr, mask_addr, switch, egress_if, egress_if_addr):
  def add_network(self, static_route_network):
    print "ROUT| Route installed\n\tTarget Net\t", \
      str(static_route_network.tar_net_addr), "\n\tTar Net CIDR\t",\
      str(static_route_network.switch_id), "\n\tAssociated swID\t",\
      str(static_route_network.mask_cidr), "\n\tEgress swport\t",\
      str(static_route_network.egress_if), "\n\tEgress if ip\t",\
      str(static_route_network.egress_if_addr)
    self.table.append(static_route_network)
  
  # Checks to see if tar_addr exists in any net in the table
  #  Docs say there is a method to do this already somewhere?
  def lookup_bool(self, tar_addr):
    for entry in self.table:
      if IPAddr(tar_addr.toUnsigned() \
          & cidr_to_netmask(entry.mask_cidr).toUnsigned()) \
          == entry.tar_net_addr:
        return True
    return False
  
  def lookup_port(self, tar_addr):
    for entry in self.table:
      if IPAddr(tar_addr.toUnsigned() \
        & cidr_to_netmask(entry.mask_cidr).toUnsigned()) \
        == entry.tar_net_addr:
        return entry.egress_if
    return False                  # FIX THIS! Will it ever occur?

  def remove_network(self):
    pass
  
  



class basic_router(object):

  def __init__ (self, connection):
    self.connection = connection
    
    # listen for PacketIn messages
    connection.addListeners(self) 
    
    self.arp_cache = {}
    # MAC & IP addresses assisnged to this device
    self.dev_mac_addrs  = [EthAddr('00:00:10:00:01:01'),EthAddr('00:00:10:00:02:01'),EthAddr('00:00:10:00:03:01')]    
    self.dev_ipv4_addrs = [IPAddr('10.0.1.1'),IPAddr('10.0.2.1'),IPAddr('10.0.3.1')]

    # IP-to-MAC dict, populateed with this device' addresses
    self.ip_to_mac = {}
    for i in range(len(self.dev_ipv4_addrs)):
      self.ip_to_mac[self.dev_ipv4_addrs[i]] = self.dev_mac_addrs[i]
      
    # WHILE ARP TX ISNT WORKING ...
    # Add hosts from mytopo to table
    self.ip_to_mac[IPAddr('10.0.1.100')] = EthAddr('00:00:10:00:01:02')
    self.ip_to_mac[IPAddr('10.0.2.100')] = EthAddr('00:00:10:00:02:02')
    self.ip_to_mac[IPAddr('10.0.3.100')] = EthAddr('00:00:10:00:03:02')
      
      
    # Populate routing table for example topology
    # A global table exists: g_ipv4_static_routes
    def example_populate_routes():
      table_net_1                 = table_network_struct()
      table_net_1.tar_net_addr    = IPAddr('10.0.1.0')
      table_net_1.mask_cidr       = 24
      table_net_1.switch          = self.connection
      table_net_1.egress_if       = 1
      table_net_1.egress_if_addr  = IPAddr('10.0.1.1')
      
      table_net_2                 = table_network_struct()
      table_net_2.tar_net_addr    = IPAddr('10.0.2.0')
      table_net_2.mask_cidr       = 24
      table_net_2.switch          = self.connection
      table_net_2.egress_if       = 2
      table_net_2.egress_if_addr  = IPAddr('10.0.2.1')
      
      table_net_3                 = table_network_struct()
      table_net_3.tar_net_addr    = IPAddr('10.0.3.0')
      table_net_3.mask_cidr       = 24
      table_net_3.switch          = self.connection
      table_net_3.egress_if       = 3
      table_net_3.egress_if_addr  = IPAddr('10.0.3.1')
      
      g_ipv4_static_routes.add_network(table_net_1)
      g_ipv4_static_routes.add_network(table_net_2)
      g_ipv4_static_routes.add_network(table_net_3)
    example_populate_routes()
    
    # Random tests
    print "RAND| LOOKUP TESTS", g_ipv4_static_routes.lookup_bool(IPAddr('10.0.1.1'))
    print "RAND| LOOKUP TESTS", g_ipv4_static_routes.lookup_bool(IPAddr('10.0.5.1'))
    print "RAND| LOOKUP TESTS", g_ipv4_static_routes.lookup_port(IPAddr('10.0.1.1'))
    print "RAND| LOOKUP TESTS", g_ipv4_static_routes.lookup_port(IPAddr('10.0.2.1'))
    print "RAND| LOOKUP TESTS", g_ipv4_static_routes.lookup_port(IPAddr('10.0.3.1'))



  def _handle_PacketIn (self, event):
    """
    Handles packet in messages from the switch.
    """
    packet = event.parsed # This is the parsed packet data.
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return

    packet_in = event.ofp

    self.l3_routing(packet, packet_in, event)

  

  def create_arp_reply(self, packet, match, event):
    """
    Constructs and returns an arp-reply from the given objects
    """
    # Ly2 addrs
    reply             = arp()
    reply.opcode      = arp.REPLY
    reply.hwdst       = match.dl_src              # reply dst mac = pkt source mac
    reply.hwsrc       = self.ip_to_mac[packet.payload.protodst] # reply src mac = ip_to_mac[ingress_interface_ip]
    # Arp attributes (not enc!)
    reply.protosrc    = packet.payload.protodst   # Ly3 Src
    reply.protodst    = match.nw_src              # Ly3 Dst
    # Encapsulate
    eth_reply         = ethernet(type=packet.ARP_TYPE, src=reply.hwsrc, dst=reply.hwdst)
    eth_reply.payload = reply
    # Construct openflow message
    msg               = of.ofp_packet_out()
    msg.data          = eth_reply.pack()          # Place encap'd arp-reply in OF msg
    msg.in_port       = event.port
    msg.actions.append(of.ofp_action_output(port=of.OFPP_IN_PORT))
    
    return(msg)

  def create_icmp_reply(self, packet, match, event):
    """
    Constructs and returns an icmp-reply from the given objects
    Requires: pox.lib.packet as lib_packet
    """
    # Upper Layers
    reply             = icmp()
    reply.type        = lib_packet.TYPE_ECHO_REPLY
    reply.payload     = packet.find("icmp").payload
    # Ly3 Encap
    ip_enc            = lib_packet.ipv4()
    ip_enc.protocol   = ip_enc.ICMP_PROTOCOL
    ip_enc.srcip      = packet.find("ipv4").dstip
    ip_enc.dstip      = packet.find("ipv4").srcip
    ip_enc.payload    = reply
    # Ly2 Encap
    eth_enc           = ethernet()
    eth_enc.src       = packet.dst
    eth_enc.dst       = packet.src
    eth_enc.type      = eth_enc.IP_TYPE
    eth_enc.payload   = ip_enc
    # Construct openflow message
    msg               = of.ofp_packet_out()
    msg.data          = eth_enc.pack()
    msg.in_port       = event.port
    msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))

    return(msg)

  def forward_packet(self):
    pass

  # Can you remove this?
  def resend_packet (self, packet_in, out_port):
    """
    Instructs the switch to resend a packet that it had sent to us.
    "packet_in" is the ofp_packet_in object the switch had sent to the
    controller due to a table-miss.
    """
    msg = of.ofp_packet_out()
    msg.data = packet_in

    # Add an action to send to the specified port
    action = of.ofp_action_output(port = out_port)
    msg.actions.append(action)

    # Send message to switch
    self.connection.send(msg)

  def l3_routing(self, packet, packet_in, event):
    """
    A catch-all method for the actions of a router.
    Can be heavily methodised
    """
    if not SUPPRESS_PACKET_MSG:
      print "PKT |", str(packet.src), "|", str(packet.dst)
      print "PKT | rx on swport: ", str(event.port)
  
    match = of.ofp_match.from_packet(packet)
    
    # -- ARP -- 
    
    # Ingress ARP Broadcast
    # if packet is: 1. of type arp, 2. an arp req, 3. addressed to an ip on this sw
    if ( match.dl_type == packet.ARP_TYPE and match.nw_proto == arp.REQUEST and match.nw_dst in self.dev_ipv4_addrs):
      print "ARP | ARP_BRDCST rx from", str(packet.src)
      
      # Does this request's Target Protocol Address match an interface address?
      if packet.payload.protodst in self.dev_ipv4_addrs:
        print "ARP |", str(packet.payload.protodst), "?", str(match.nw_src), "\n\tTPA is an IP on this switch!"
      
        # Update ARP-Cache
        if not match.nw_src in self.ip_to_mac:
          self.ip_to_mac[match.nw_src] = match.dl_src
          print "ARP | Cache, new install:\n\t", str(match.nw_src), " -> ", str(self.ip_to_mac[match.nw_src])

        # Send ARP Reply Frame
        event.connection.send(self.create_arp_reply(packet, match, event))
        print "ARP | Reply tx"
        
      else:
        pass  # Drop the frame

    # -- ICMP Reply -- 

    # if packet is: of type icmp
    if packet.find("icmp"):
    
      # if packet is addressed to an interface on this switch
      if match.nw_dst in self.dev_ipv4_addrs:
        print "ICMP| Ping rx for this sw:", str(match.nw_dst)

        # Send ICMP Reply Frame
        print "ICMP| Pong tx"
        event.connection.send(self.create_icmp_reply(packet, match, event))
      
      # if icmp dst ip is in the routing table
      elif g_ipv4_static_routes.lookup_bool(match.nw_dst):
        print "ICMP| rx icmp has tar ip on table!"
        
        # Implement routing, i.e. install a flow
        
        dst_ip_addr = packet.next.dstip
        
        # if dstip MAC is known
        if dst_ip_addr in self.ip_to_mac:    
          print "ICMP| dst ip is in arp cache"
        
          fwdport = g_ipv4_static_routes.lookup_port(dst_ip_addr)

          msg = of.ofp_flow_mod()

          msg.actions.append(of.ofp_action_dl_addr.set_dst(self.ip_to_mac[dst_ip_addr]))
          msg.actions.append(of.ofp_action_output(port = fwdport))

          msg.match           = of.ofp_match.from_packet(packet, event.port)
          msg.match.dl_src    = None

          msg.command       = of.OFPFC_ADD
          msg.idle_timeout  = FLOW_IDLE_TIMEOUT
          msg.hard_timeout  = of.OFP_FLOW_PERMANENT
          msg.buffer_id     = event.ofp.buffer_id

          self.connection.send(msg)
          
        else:
          pass
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      # if icmp dst is an off-table network
      # - drop?
      # - fwd out default? what is default?
      if packet.find("ipv4").dstip:
        pass




    if packet.find("udp") or packet.find("tcp"):
      pass



# Utility Functions
def subnet_check(tar_addr, net_mask, net_addr):
  return IPAddr(tar_addr.toUnsigned()&net_mask.toUnsigned()) == net_addr


# Globals

log = core.getLogger()
g_ipv4_static_routes = ipv4_static_route_table()  # Global Routing Table (Ly3 SDN FUARRRR)



# Launches the switch (Called by POX)

def launch():
	
  def start_switch(event):
	  
    log.debug("Controlling %s" % (event.connection,))
    basic_router(event.connection)
    
  core.openflow.addListenerByName("ConnectionUp", start_switch)
