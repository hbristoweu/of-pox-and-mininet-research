from mininet.topo import Topo

class MyTopo( Topo ):
  "Simple topology example."

  def __init__( self ):
    "Create custom topo."

    # Initialize topology
    Topo.__init__( self )

    host1 = self.addHost('h1', ip="10.0.1.101/24", defaultRoute = "via 10.0.1.1", mac="00:00:10:00:01:02")
    host2 = self.addHost('h2', ip="10.0.1.102/24", defaultRoute = "via 10.0.1.1", mac="00:00:10:00:02:02")
    host3 = self.addHost('h3', ip="10.0.1.103/24", defaultRoute = "via 10.0.1.1", mac="00:00:10:00:03:02")
    host4 = self.addHost('h3', ip="10.0.1.104/24", defaultRoute = "via 10.0.1.1", mac="00:00:10:00:04:02")
    s1 = self.addSwitch('s1')
    s2 = self.addSwitch('s2')
    s3 = self.addSwitch('s3')
    s4 = self.addSwitch('s4')

    self.addLink(host1, s1)
    self.addLink(host2, s2)
    self.addLink(host3, s3)
    self.addLink(host4, s4)
    
    self.addLink(s1,s2)
    self.addLink(s1,s4)
    
    self.addLink(s2,s4)
    self.addLink(s2,s3)
    
    self.addLink(s3,s4)
    
    
    

topos = { '4sw_w_loops': ( lambda: MyTopo() ) }
