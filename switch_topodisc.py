# ./pox.py log.level --DEBUG misc.switch_topodisc openflow.discovery

from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.util import dpid_to_str as dts
import networkx as nx



class Switch_o(object):

  def __init__ (self, connection):
    print "__init__"
    self.dpid = connection.dpid
    self.neighbors = []
    def startup():
      core.openflow.addListeners(self, priority = 0)
      core.openflow_discovery.addListeners(self)
    core.call_when_ready(startup, ('openflow','openflow_discovery'))

  def _handle_PacketIn (self, event):
    
    packet = event.parsed
    #print "_handle_PacketIn | ", packet

  def _handle_LinkEvent(self, event):
    l = event.link

    #print dpid_to_str(self.dpid), 'link added is %s'%event.added
    #print dpid_to_str(self.dpid), 'link removed is %s' %event.removed
    #print dpid_to_str(self.dpid), 'switch1 %s' %dpid_to_str(l.dpid1)
    #print dpid_to_str(self.dpid), 'port1 %d' %l.port1
    #print dpid_to_str(self.dpid), 'switch2 %s' %dpid_to_str(l.dpid2)
    #print dpid_to_str(self.dpid), 'port2 %d' %l.port2

    print dts(self.dpid), "|", dts(l.dpid1), ":", l.port1, \
      "-", dts(l.dpid2), ":", l.port2

    if not g_logi_topo.has_node(l.dpid2):
      g_logi_topo.add_node(l.dpid2)
      print dts(self.dpid),"| new node:", l.dpid2, "| nodes:", g_logi_topo.nodes()
      rebuild_global_spanningtree(g_logi_topo)

    if not g_logi_topo.has_edge(l.dpid1,l.dpid2):
      g_logi_topo.add_edge(l.dpid1,l.dpid2)
      print dts(self.dpid),"| new edge:", l.dpid1, l.dpid2, "| edges", g_logi_topo.edges()
      rebuild_global_spanningtree(g_logi_topo)



g_logi_topo = nx.Graph()
g_logi_spanningtree = nx.Graph()



def rebuild_global_spanningtree(g_logi_topo):
  g_logi_spanningtree = lazy_weightless_spanning_tree(g_logi_topo)
  print "Global spanningtree rebuilt.", g_logi_spanningtree.edges()

def lazy_weightless_spanning_tree(G, root=None):
  
  if not root:
    root = G.nodes()[0]
    
  def c(n, e):
    return e[0] if e[1] == n else e[1]

  E = []; H = [root]; O = []; R = nx.Graph()

  edges = list(filter(lambda p: root in p, G.edges() ))
  for e in edges:
    R.add_edge(e[0], e[1])
    H.append( c(root, e) )
    O.append( c(root, e) )

  while True:
    if O:
      n_edges = list(filter(lambda p: O[0] in p, G.edges() ))
      for e in n_edges:
        if c(O[0], e) not in O and c(O[0], e) not in H:
          R.add_edge(e[0], e[1])
          H.append(c(O[0], e))
          O.append(c(O[0], e))
      O.remove(O[0])
    else:
      break
 
  return R

def launch():
	
  def start_switch(event):

    Switch_o(event.connection)
  
  core.openflow.addListenerByName("ConnectionUp", start_switch)
