#! ./pox.py log.level misc.switch_blacklist_topodisc openflow.discovery

# ----- RESEARCH NOTES ----- 

#  Stackoverflow - Disabling OFP ports; mentions ofp-port-mod message
#  http://stackoverflow.com/questions/30617656/how-can-i-activate-the-port-disabled-blocked-by-stp-using-openflow-and-ryu-con

#  Ryu controller's OFPPortMod
#  http://ryu.readthedocs.io/en/latest/ofproto_v1_3_ref.html#ryu.ofproto.ofproto_v1_3_parser.OFPPortMod

#  Openflow Protocol Specification - 
#   page 64 - 7.2.1.1 Port Description Structures
#   "Current state of the physical port. These are not configurable 
#   from the controller" :((((
#   "The OFPPS_BLOCKED bit indicates that a switch protocol outside of 
#    OpenFlow, such as 802.1D Spanning Tree, is preventing the use of 
#    that port with OFPP_FLOOD."
#   page 243 - B.6.8 802.1D Spanning Tree
#    OFP implements support for 802.1D that may be present in Trad/SDN
#    hybrid topologies.
#  https://www.opennetworking.org/images/stories/downloads/sdn-resources/onf-specifications/openflow/openflow-switch-v1.5.0.noipr.pdf

#  POX repo, spanning tree example - demonstrates portmod
#   of.ofp_port_mod(
#    port_no = p.port_no
#    hw_addr = p.hw_addr
#    config  = of.OFPPC_NO_FLOOD
#    mask    = of.OFPPC_NO_FLOOD
#  https://github.com/noxrepo/pox/blob/carp/pox/openflow/spanning_tree.py

#  Utility Functions: Changing Port Configuration (What is 'mask' ? ^ )
#   The mask is always set to the OFPPC_PORT_DOWN flag since this is the 
#   setting we want to affect. The config is set to the same value if 
#   set_port_down is True.
#  http://ofdpa.com/2015/06/12/utility-functions-changing-port-configuration/

#  NetworkX
#   Arbitrary data can be included with the definition of an edge
#    G.add_edge(2,3,weight=5)
#   Which can be accessed via data=True arg
#    G.edges(data=True) # default edge data is {} (empty dictionary)
#     [(0, 1, {}), (1, 2, {}), (2, 3, {'weight': 5})]
#  https://networkx.github.io/documentation/networkx-1.10/reference/generated/networkx.Graph.edges.html



# ----- DESIGN NOTES ----- 



# What is the point of blocking state where only management traffic 
#  can egress when there should be no management traffic in the first
#  place? 802.1D is redundant now, as are most protocols. 
#  Egress traffic from the port can be dropped with:
#   ofp_port_config OFPPC_NO_FWD
#   This effectively disables the fwding capability of a swport

# A new design following some reasearch:
#  Employ a whitelist system for ports.
#  1. Block all ports and calculate the spanning tree
#  2. For each link as an edge, if in the spanning tree, unblock it
#  For every port change, block ports, recalc tree, and whitelist ports
#
# g.edges() 
#  edge_list (list of edge tuples)

# Could you install a flow to drop traffic that egresses a port based
#  on its content vlan? e.g.
#   match = {vlan = 1, egress_port = 3}
#   action = drop/null-switch

# pox/openflow/spanning_tree.py uses ofppc OFPPC_NO_FLOOD
#  "constructs a spanning tree, and then disables flooding on switch
#  ports that aren't on the tree by setting their NO_FLOOD bit.  The 
#  result is that topologies with loops no longer turn your network into 
#  useless hot packet soup."

from pox.core import core
from pox.openflow.libopenflow_01 import *
import pox.openflow.libopenflow_01 as of
from pox.lib.util import dpid_to_str as dts
import networkx as nx
from time import sleep
from copy import deepcopy, copy



class Switch_o(object):

  def __init__ (self, connection):
    print "__init__"
    self.dpid = connection.dpid
    self.connection = connection
    self.neighbors = []
    def startup():
      core.openflow.addListeners(self, priority = 0)
      core.openflow_discovery.addListeners(self)
    core.call_when_ready(startup, ('openflow','openflow_discovery'))

  def _handle_PacketIn (self, event):
    
    packet = event.parsed
    #print "_handle_PacketIn | ", packet

  def _handle_LinkEvent(self, event):
    l = event.link

    #print dpid_to_str(self.dpid), 'link added is %s'%event.added
    #print dpid_to_str(self.dpid), 'link removed is %s' %event.removed
    #print dpid_to_str(self.dpid), 'switch1 %s' %dpid_to_str(l.dpid1)
    #print dpid_to_str(self.dpid), 'port1 %d' %l.port1
    #print dpid_to_str(self.dpid), 'switch2 %s' %dpid_to_str(l.dpid2)
    #print dpid_to_str(self.dpid), 'port2 %d' %l.port2

    print dts(self.dpid), "|", dts(l.dpid1), ":", l.port1, \
      "-", dts(l.dpid2), ":", l.port2
      
    # For each update, add new tree componant, rebuild spanning tree,
    # block ALL ports, unblock ports present in spanning tree
    # - This can be way optimised

    # new node - could be a neighbor
    if not g_logi_topo.has_node(l.dpid2):
      if l.dpid1 == self.dpid:
        self.neighbors.append(l.dpid2)
        print dts(self.dpid), "|",self.dpid,"| new neigh:",l.dpid2, \
          "\n\t", self.neighbors
      add_node_g_logi_topo(l.dpid2)
      
    # new link
    if not g_logi_topo.has_edge(l.dpid1,l.dpid2):
      g_logi_topo.add_edge(l.dpid1,l.dpid2)
      print dts(self.dpid),"| new edge:", l.dpid1, l.dpid2, "| edges", g_logi_topo.edges()
      rebuild_global_spanningtree(g_logi_topo)
    
    self.block_port(l.port1)
    
    #sleep(1)
    
    print "FULL_TREE:\t", g_logi_topo.edges()
    print "SPT:\t\t", g_logi_spanningtree.edges()
    print "\t", l.dpid1,l.dpid2

    if g_logi_spanningtree.has_edge(l.dpid1,l.dpid2):
      print "SPANNING TREE - this edge exists - unblocking"
      self.unblock_port(l.port1)



  def block_port(self, swport_no):
    """
    THIS IS HOW YOU GET SWITCHPORT OBJECTS!!!!!!!!!!!!!
    """
    for p in self.connection.ports.itervalues():
      if p.port_no == swport_no:
      
        print dts(self.dpid),"| FWDBLK: PORT:", swport_no
        msg = of.ofp_port_mod(
          port_no = p.port_no,
          hw_addr = p.hw_addr,
          mask    = OFPPC_NO_FLOOD,
          config  = OFPPC_NO_FLOOD )
        self.connection.send(msg)

  def unblock_port(self, swport_no):
    """
    THIS IS HOW YOU GET SWITCHPORT OBJECTS!!!!!!!!!!!!!
    ofp doc pp. 243: OFPPC_NO_FLOOD definition
    """
    for p in self.connection.ports.itervalues():
      if p.port_no == swport_no:
      
        print dts(self.dpid),"| FWDUNBLK: PORT:", swport_no
        msg = of.ofp_port_mod(
          port_no = p.port_no,
          hw_addr = p.hw_addr,
          mask    = OFPPC_NO_FLOOD,
          config  = 0 )
        self.connection.send(msg)



g_logi_topo = nx.Graph()
g_logi_spanningtree = nx.Graph()



def add_node_g_logi_topo(node):
  g_logi_topo.add_node(node)
  rebuild_global_spanningtree(g_logi_topo)

def rebuild_global_spanningtree(g_logi_topo):
  global g_logi_spanningtree 
  g_logi_spanningtree = lazy_weightless_spanning_tree(g_logi_topo)
  print id(g_logi_spanningtree), "|", id(g_logi_topo)
  print "Global spanningtree rebuilt.", type(g_logi_spanningtree), g_logi_spanningtree.edges()

def lazy_weightless_spanning_tree(G, root=None):
  """
  Lazy, Weightless Spanning Tree
  
  A Spanning-Tree algorithm I came up with
  No system of priority is used. A n-ary tree is formed through
   procedural discovery starting from a given root node for some graph
  The implementation of this program uses the python graphing module
   networkx.
  """
  if not root:
    root = G.nodes()[0]
    
  def c(n, e):
    return e[0] if e[1] == n else e[1]

  E = []; H = [root]; O = []; R = nx.Graph()

  edges = list(filter(lambda p: root in p, G.edges() ))
  for e in edges:
    R.add_edge(e[0], e[1])
    H.append( c(root, e) )
    O.append( c(root, e) )

  while True:
    if O:
      n_edges = list(filter(lambda p: O[0] in p, G.edges() ))
      for e in n_edges:
        if c(O[0], e) not in O and c(O[0], e) not in H:
          R.add_edge(e[0], e[1])
          H.append(c(O[0], e))
          O.append(c(O[0], e))
      O.remove(O[0])
    else:
      break
 
  print "LAZY WEIGHTLESS: NEW TREE BUILT:\n\t", R.edges()
  print type(R), "|", id(R), id(G), R == G
  
  return R

def launch():
	
  def start_switch(event):

    Switch_o(event.connection)
  
  core.openflow.addListenerByName("ConnectionUp", start_switch)
