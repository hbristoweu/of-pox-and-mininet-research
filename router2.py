# Second attempt at a Pox router

# Research
#  https://github.com/mininet/openflow-tutorial/wiki/Router-Exercise        (Pox/mn/OF Router Exercise)
#  https://pieknywidok.blogspot.co.uk/2012/08/arp-and-ping-in-pox-building-pox-based.html (ARP + ICMP Response Example)
#  http://www.tcpipguide.com/free/t_ARPAddressSpecificationandGeneralOperation-2.htm      (ARP)
#  http://www.tcpipguide.com/free/t_ARPCaching.htm                          (ARP Caching)
#  http://archive.openflow.org/doc/gui/org/openflow/protocol/Match.html     (match obj specification)
#  http://rlenglet.github.io/openfaucet/match.html                          (match obj explanation)
#  http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml  (Ly3 Proto numbers re. match.nw_proto)
#  pox/pox/forwarding/l2_multi.py                                            (Example of ICMP DEST UNREACHABLE)

# Commands
#  $ ovs-ofctl dump-flows s1    View installed flows

# I purposefully introduce C-struct like approach to valuing objects
#  for the sake of reference and clarity. Inline definitions are not
#  benefitial to the learning process.

# Two approaches to routing tables have become evident in SDN networks
# - Object-unique tables    Per Switch-Object tables
# - Global tables           Globally accessable tables (Used here)
#   - Every network installed is associated with a switch dpid
# In contrast to the last router, the routing table does not store
#  information regarding swports. Instead, a new object-specific
#  approach is taken. See: Swport object, basic_router.ip_to_swport()

import struct
from pox.core import core
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as lib_packet # packet.arp .icmp .ethernet   etc...                                      
from pox.lib.addresses import IPAddr, EthAddr, cidr_to_netmask  # Address types

# Constants

SUPPRESS_MBCAST_MSG   = True
SUPPRESS_PACKET_MSG   = True
FLOW_IDLE_TIMEOUT     = 10



class Swport(object):
  """
  Switchport object. switch objects can create lists of swport objects
  for easier handling of per-port actions.
  """
  def __init__(self, parent_swport_no, dl_addr, nw_addr, nw_cidr, parent_dpid):
    self.parent_swport_no   = parent_swport_no
    self.dl_addr            = dl_addr
    self.nw_addr            = nw_addr
    self.nw_cidr            = nw_cidr
    self.parent_dpid        = parent_dpid
    
  def __str__(self):
    return "sw:" + str(self.parent_dpid) + \
      ",swport:" + str(self.parent_swport_no) + \
      ",mac:" + str(self.dl_addr) + \
      ",ip:" + str(self.nw_addr)



class Routing_table_network(object):
  sw_dpid         = None    # basic_router.dpid
  nw_addr         = None    # IPAddr
  mask_cidr       = None    # Int
  egress_ip       = None    # IPAddr

  def __init__(self, sw_dpid=None, nw_addr=None, mask_cidr=None, egress_ip=None):
    self.sw_dpid    = sw_dpid
    self.nw_addr    = nw_addr
    self.mask_cidr  = mask_cidr 
    self.egress_ip  = egress_ip

  def __str__(self):
    return "sw:" + str(self.sw_dpid) + ",nw:" + str(self.nw_addr) + \
    "/" + str(self.mask_cidr) + ",via:" + str(self.egress_ip)



class G_routing_table(object):
  """
  'local' in a method name indicates a calling object must provide it's
  dpid, allowing for specific lookups.
  """

  networks = []

  def __int__(self):
    pass

  def print_table(self):
    print "RTBL | Routing Table"
    for net in self.networks:
      print "     |   ", net

  def add_network(self, rtn):
    self.networks.append(rtn)
    print "RTBL | Installed new route: ", str(rtn)

  def lookup_local_bool(self, caller_dpid, tar_addr):
    """
    Table lookup for tar_addr over all networks specific to the calling 
    switch objects dpid: caller_dpid
    If the address is within a network on the table, returns True
    else returns False
    """
    for net in self.networks:
      if net.sw_dpid == caller_dpid \
        and subnet_check(tar_addr, net.mask_cidr, net.nw_addr):
        
        return True
    return False

  def lookup_local_gateway(self, caller_dpid, tar_addr):
    """
    Looks for the tar_addr containing subnet with the highest mask and 
    returns the egress_ip attribute for that network object.
    This call is exclusive to the given caller dpid
    """
    rtn_gw		= None
    lst_mask	= 0
    for net in self.networks:
      if subnet_check(tar_addr, net.mask_cidr, net.nw_addr) \
        and net.mask_cidr > lst_mask:
        
        rtn_gw		= net.egress_ip
        lst_mask	= net.mask_cidr
        
    return rtn_gw   



class basic_router(object):

  def __init__ (self, connection):
    """
    Constructor for this ofswitch
    """
    self.connection = connection
    self.dpid = connection.dpid
    connection.addListeners(self)
    
    print '\n'*3, "POX  | New switch instance: switch #", self.dpid, "is now online."
    
    # Addresses for interfaces on this ofsw
    self.dev_mac_addrs  = [EthAddr('00:00:10:00:01:01'),EthAddr('00:00:10:00:02:01'),EthAddr('00:00:10:00:03:01')]    
    self.dev_ipv4_addrs = [IPAddr('10.0.1.1'),IPAddr('10.0.2.1'),IPAddr('10.0.3.1')]
    self.dev_ports = {}
    
    for i in range(len(self.dev_mac_addrs)):
      self.dev_ports[i] = Swport(i+1, self.dev_mac_addrs[i], self.dev_ipv4_addrs[i], 24, self.dpid)
    
    # ARP+Port cache
    self.arp_cache = {}   # ip -> mac dict
    
    # IP to Port cache
    # Used to find egress swports for next-hops
    self.port_cache = {
      IPAddr('0.0.0.0'):self.dev_ports[0],
      IPAddr('10.0.1.1'):self.dev_ports[0],
      IPAddr('10.0.2.1'):self.dev_ports[1],
      IPAddr('10.0.3.1'):self.dev_ports[2]	}
    
    # Populate data structures
    for i in range(len(self.dev_ipv4_addrs)):
      self.arp_cache[self.dev_ipv4_addrs[i]] = self.dev_mac_addrs[i]
    self.dev_port_count = len(self.dev_ports)
    
    # Install static rules
    net1            = Routing_table_network()
    net1.sw_dpid    = self.dpid
    net1.nw_addr    = ip_to_network_addr(IPAddr('10.0.1.1'), cidr_to_netmask(24))
    net1.mask_cidr  = 24
    net1.egress_ip  = IPAddr('10.0.1.1')
    net2            = Routing_table_network()
    net2.sw_dpid    = self.dpid
    net2.nw_addr    = ip_to_network_addr(IPAddr('10.0.2.1'), cidr_to_netmask(24))
    net2.mask_cidr  = 24
    net2.egress_ip  = IPAddr('10.0.2.1')
    net3            = Routing_table_network()
    net3.sw_dpid    = self.dpid
    net3.nw_addr    = ip_to_network_addr(IPAddr('10.0.3.1'), cidr_to_netmask(24))
    net3.mask_cidr  = 24
    net3.egress_ip  = IPAddr('10.0.3.1')

    g_routing_table.add_network(net1)
    g_routing_table.add_network(net2)
    g_routing_table.add_network(net3)
    
    # Print the Routing Table
    
    g_routing_table.print_table()
    
    self.___TESTS()
  
  def ___TESTS(self):
    
    # # # TESTS # # #
    
    print "### gw lookup tests"
    
    supergw = Routing_table_network(
      self.dpid, 
      ip_to_network_addr(IPAddr('10.0.1.1'), cidr_to_netmask(16)),
      16,
      IPAddr('10.0.3.80') )
    g_routing_table.add_network(supergw)
    g_routing_table.print_table()
    print "gw lookup = 10.0.1.1 ?=", g_routing_table.lookup_local_gateway(self, IPAddr('10.0.1.1'))
    
    print "### swport find tests"
    
    swp = self.ip_to_swport(IPAddr('10.0.1.1'))
    print "swport find tests: ", str(swp)
    swp = self.ip_to_swport(IPAddr('10.0.3.1'))
    print "swport find tests: ", str(swp)
    swp = self.ip_to_swport(IPAddr('10.0.2.1'))
    print "swport find tests: ", str(swp)
    
    print "### lookup tests"
    
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.1.1'))
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.1.100'))
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.2.1'))
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.2.100'))
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.3.1'))
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.3.100'))
    print "Lookup bool test: ", g_routing_table.lookup_local_bool(self.dpid, IPAddr('10.0.4.1'))
    
    print "### TX ARP tests"
    
    self.tx_arp(self.ip_to_swport(IPAddr('10.0.1.100')), IPAddr('10.0.1.100'))
    self.tx_arp(self.ip_to_swport(IPAddr('10.0.3.100')), IPAddr('10.0.1.100'))
    
    print "### check_cache"
    
    print "check_cache true == ", self.check_cache(IPAddr('10.0.1.1'))
    print "check_cache false == ", self.check_cache(IPAddr('10.0.5.1'))
    print "check_cache true == ", self.check_cache(IPAddr('10.0.1.1'), EthAddr('00:00:10:00:01:01'))
    print "check_cache true (installed) == ", self.check_cache(IPAddr('10.0.4.1'), EthAddr('00:00:10:00:04:01'))
    
    print "### self.port_cache"
    
    #for nw_addr in self.dev_ipv4_addrs:
    #  print "nw_addr:", str(nw_addr), " via port:", self.port_cache[nw_addr]
      
    for k_nw_addr, v_swport in self.port_cache.iteritems():
      print "nw_addr:", k_nw_addr, "\tvia port:", v_swport

  def ip_to_swport(self, nw_addr):
    """
    Iterates over all configured swports to determine if a subnet is
    directly reachable from each port.
    """
    # If there is a Swport obj in a the same subnet for the given addr
    for i in xrange(self.dev_port_count):
      if ip_to_network_addr(self.dev_ports[i].nw_addr, self.dev_ports[i].nw_cidr) == \
        ip_to_network_addr(nw_addr, self.dev_ports[i].nw_cidr):
        
        return self.dev_ports[i]
    
    # If the IP is installed in the port_cache
    for k_nw_addr, v_swport in self.port_cache.iteritems():
      if k_nw_addr == nw_addr:
        return v_swport
    
    return False

  def check_cache(self, nw_addr, dl_addr=None):
    """
    Checks a given IP against the arp_cache, returning a bool resp.
    Takes an optional second param for a ly2 address to install should
    the ly3 address not be cached.
    """
    if nw_addr in self.arp_cache:
	  return True
    else:
	  if dl_addr:
	    print "ARP_C| New cache entry: ", str(nw_addr), str(dl_addr)
	    self.arp_cache[nw_addr] = dl_addr
	    return True
	  else:
	    return False

  def tx_arp_reply(self, packet, match, event):
    """
    Constructs and returns an arp-reply from the given objects
    Requires: pox.lib.packet as lib_packet
    """
    # Ly2 addrs
    reply             = lib_packet.arp()
    reply.opcode      = lib_packet.arp.REPLY
    reply.hwdst       = match.dl_src              # reply dst mac = pkt source mac
    reply.hwsrc       = self.arp_cache[packet.payload.protodst] # reply src mac = ip_to_mac[ingress_interface_ip]
    # Arp attributes (not enc!)
    reply.protosrc    = packet.payload.protodst   # Ly3 Src
    reply.protodst    = match.nw_src              # Ly3 Dst
    # Encapsulate
    eth_reply         = lib_packet.ethernet()
    eth_reply.type    = packet.ARP_TYPE
    eth_reply.src     = reply.hwsrc
    eth_reply.dst     = reply.hwdst
    eth_reply.payload = reply
    # Construct openflow message
    msg               = of.ofp_packet_out()
    msg.data          = eth_reply.pack()          # Place encap'd arp-reply in OF msg
    msg.in_port       = event.port
    msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
    
    return(msg)

  def tx_icmp_reply(self, packet, match, event):
    """
    Constructs and returns an icmp-reply from the given objects
    Requires: pox.lib.packet as lib_packet
    """
    # Upper Layers
    reply             = lib_packet.icmp()
    reply.type        = lib_packet.TYPE_ECHO_REPLY
    reply.payload     = packet.find("icmp").payload
    # Ly3 Encap
    ip_enc            = lib_packet.ipv4()
    ip_enc.protocol   = ip_enc.ICMP_PROTOCOL
    ip_enc.srcip      = packet.find("ipv4").dstip
    ip_enc.dstip      = packet.find("ipv4").srcip
    ip_enc.payload    = reply
    # Ly2 Encap
    eth_enc           = lib_packet.ethernet()
    eth_enc.type      = eth_enc.IP_TYPE
    eth_enc.src       = packet.dst
    eth_enc.dst       = packet.src
    eth_enc.payload   = ip_enc
    # Construct openflow message
    msg               = of.ofp_packet_out()
    msg.data          = eth_enc.pack()
    msg.in_port       = event.port
    msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))

    return(msg)

  def tx_icmp_dest_unreachable(self, packet, match, event):
    """
    Generates a Destiniation Unreachable ICMP reply
    Requires: pox.lib.packet as lib_packet
    """
    # Upper Layers
    reply             = lib_packet.icmp()
    reply.type        = lib_packet.TYPE_DEST_UNREACH
    reply.code        = lib_packet.ICMP.CODE_UNREACH_HOST
    # Dest. Unreachable has a specific payload we must construct
    t_ip = event.parsed.find('ipv4')
    t_pk = t_ip.pack()[:t_ip.hl * 4 + 8]
    reply.payload     = struct.pack("!HH", 0,0) + t_pk
    # Ly3 Encap
    ip_enc            = lib_packet.ipv4()
    ip_enc.protocol   = ip_enc.ICMP_PROTOCOL
    ip_enc.srcip      = packet.find("ipv4").dstip
    ip_enc.dstip      = packet.find("ipv4").srcip
    ip_enc.payload    = reply
    # Ly2 Encap
    eth_enc           = lib_packet.ethernet()
    eth_enc.src       = packet.dst
    eth_enc.dst       = packet.src
    eth_enc.type      = eth_enc.IP_TYPE
    eth_enc.payload   = ip_enc
    # Construct openflow message
    msg               = of.ofp_packet_out()
    msg.data          = eth_enc.pack()
    msg.in_port       = event.port
    msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))

    return(msg)

  def tx_arp(self, swport, tar_dl_addr):
    """
    Formulates and sends an ARP request for the target IP tar_dl_addr on 
    the egress Swport object swport
    """
    request           = lib_packet.arp()
    request.opcode    = lib_packet.arp.REQUEST
    request.hwdst     = EthAddr('FF:FF:FF:FF:FF:FF')
    request.hwsrc     = swport.dl_addr
    request.protodst  = tar_dl_addr
    request.protosrc  = swport.nw_addr
    
    eth_req           = lib_packet.ethernet()
    eth_req.type      = lib_packet.ethernet.ARP_TYPE
    eth_req.src       = request.hwsrc
    eth_req.dst       = request.hwdst
    eth_req.payload   = request
    
    msg               = of.ofp_packet_out()
    msg.data          = eth_req.pack()
    msg.actions.append(of.ofp_action_output(port = swport.parent_swport_no))
    
    self.connection.send(msg)

  def rx_arp(self, packet, match, event):
    """
    What to do with a received ARP packet
    """
    if packet.payload.opcode == lib_packet.arp.REQUEST:
    
      if packet.payload.protodst in self.dev_ipv4_addrs:
      
        #if not match.nw_src in self.arp_cache:
        #  self.arp_cache[match.nw_src] = match.dl_src
        self.check_cache(match.nw_src, match.dl_src)
          
        return self.tx_arp_reply(packet, match, event)

    if packet.payload.opcode == lib_packet.arp.REPLY:
    
       if not match.nw_src in self.arp_cache:
          #print "ARP  | rx REPLY, installing to cache"
          #self.arp_cache[match.nw_src] = match.dl_src
          self.check_cache(match.nw_src, match.dl_src)

  def rx_icmp(self, packet, match, event):
    """
    What to do with an ICMP packet
    """
    # if packet is addressed to an interface on this switch
    if match.nw_dst in self.dev_ipv4_addrs:
      return self.tx_icmp_reply(packet, match, event)

    #elif icmp dst ip is in the routing table
    elif g_routing_table.lookup_local_bool(self.dpid, match.nw_dst):
      return self.fwd_icmp(packet, match, event)
    
    #elif icmp dst is in an unreachable network: DEST_UNREACH
    elif not g_routing_table.lookup_local_bool(self.dpid, match.nw_dst):
      print "ICMP | Replying dst unreachable for ", str(match.nw_dst)
      return self.tx_icmp_dest_unreachable(packet, match, event)
      
  def fwd_icmp(self, packet, match, event):

    if packet.find("ipv4").dstip in self.arp_cache:
      
      egress_swport = self.ip_to_swport(packet.find("ipv4").dstip)
      
      msg = of.ofp_flow_mod()
      
      msg.actions.append(of.ofp_action_dl_addr.set_dst(self.arp_cache[packet.find("ipv4").dstip]))
      msg.actions.append(of.ofp_action_output(port = egress_swport.parent_swport_no))
      
      msg.match           = of.ofp_match.from_packet(packet, event.port)
      msg.match.dl_src    = None  # Wildcard
      
      msg.command       = of.OFPFC_ADD
      msg.idle_timeout  = FLOW_IDLE_TIMEOUT
      msg.hard_timeout  = of.OFP_FLOW_PERMANENT
      msg.buffer_id     = event.ofp.buffer_id
      
      #self.connection.send(msg)
      return msg
      
    else: 
      swport = self.ip_to_swport(packet.find("ipv4").dstip)
      self.tx_arp(swport, packet.find("ipv4").dstip)

  def fwd_udp(self, packet, match, event):
    pass

  def fwd_tcp(self, packet, match, event):
    pass

  def _handle_PacketIn (self, event):
    """
    Entry point for new packets
    """
    packet = event.parsed
    
    if not packet.parsed:
      g_log.warning("Ignoring incomplete packet")
      return

    packet_in = event.ofp

    self.core_logic(packet, packet_in, event)

  def core_logic(self, packet, packet_in, event):
    
    match = of.ofp_match.from_packet(packet)
    
    # Ly2 Protocols

    if packet.find("arp"):
    
      act = self.rx_arp(packet, match, event)
      
      if act:
        self.connection.send(act)  
    
    
    
    # Ly3 Protocols
    
    if packet.find("udp"):
      pass
    
    
    
    if packet.find("tcp"):
      pass
    
    
    
    if packet.find("icmp"):
    
      act = self.rx_icmp(packet, match, event)

      if act:
        self.connection.send(act)





# Utility Functions

def subnet_check(tar_addr, net_mask, net_addr):
  """
  Performs CIDR bitwise checks to determine if tar_addr is an address
  in the subnet net_addr under the network mask net_mask
  net_mask can be given as a POX IPAddr type or Integer (for CIDR)
  """
  if type(net_mask) == int:
    net_mask = cidr_to_netmask(net_mask)
  return IPAddr(tar_addr.toUnsigned()&net_mask.toUnsigned()) == net_addr

def ip_to_network_addr(tar_addr, net_mask):
  """
  Returns the network address of the given IP tar_addr and mask
  net_mask
  net_mask can be given as a POX IPAddr type or Integer (for CIDR)
  """
  if type(net_mask) == int:
    net_mask = cidr_to_netmask(net_mask)
  return IPAddr(tar_addr.toUnsigned()&net_mask.toUnsigned())





# Global variables

g_routing_table   = G_routing_table()

g_log             = core.getLogger()





# Entrypoint - Called by Pox

def launch():
	
  def start_switch(event):
	  
    g_log.debug("Controlling %s" % (event.connection,))
    basic_router(event.connection)
    
  core.openflow.addListenerByName("ConnectionUp", start_switch)
